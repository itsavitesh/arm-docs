## Header Details ##
* Contact Number : +91-9993417435
* Email: armsolarsolutions@gmail.com
* FB Link: https://www.facebook.com/armsolarsolutions
* Instagram Link: https://www.instagram.com/plugintothesun/

## Banner ##
### ARM Logo ###
![](images/arm-logo.png)


## Partners ##
### Headline ###
Our Implementation Partners and Organizations
### Statement ###
We are Franchised partners and dealers for Tier-1 Manufacturer and also registered under various Indian Government bodies.
![](images/Waaree-logo.png)
![](images/waacab-logo.png)
![](images/luminous-logo.jpg)
![](images/Okaya-logo.png)
![](images/mnre.jpg)
![](images/msme.png)
![](images/startupindia.png)
![](images/tier-1.png)


## Moving Numbers ##
* Projects Completed - 43 
![](images/projects-icon.png)
* Trees Saved - 1092
![](images/tree-icon.png)
* Ton Co2 Saved - 3
![](images/co2-icon.png)
* Installed - 4.2Mw
![](images/energy-icon.png)


## News Ticker ##
* UK PM praises India's 'incredible' solar power strides at climate summit (https://economictimes.indiatimes.com/industry/energy/power/uk-pm-praises-indias-incredible-solar-power-strides-at-climate-summit/articleshow/79707204.cms)
* Waaree Energies launches financing facility for solar power projects (https://energy.economictimes.indiatimes.com/news/renewable/waaree-energies-launches-financing-facility-for-solar-power-projects/70393207)
* Mizoram enters solar map of country with 2-MW photovoltaic (https://economictimes.indiatimes.com/industry/energy/power/mizoram-enters-solar-map-of-country-with-2-mw-photovoltaic/articleshow/79580922.cms)


## Contact Form ##
* Name (Mandatory)
* Pin Code (Mandatory, Validation, will be awesome if it can pick automatically)
* Contact Number (Mandatory, phone number validation)
* Email (Optional, Email Validation)
* Project Type (Mandatory, Drop Down with Residential, Commercial)
* Service Interested in (Mandatory, Dropdown with Free Consultation, Solar Panel, Solar Wires, Solar Batteries, Solar UPS, Structure, ACDB Box, Installation, Repair, Other)
* Description (Optional, Text Area)


## Images Slider ##
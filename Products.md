# Products #

List of ARM SOlar Products and details

## Solar Modules ##
### Brands Available- Waaree, Luminous, Okaya ###

*	Glass with anti-reflective coating improves transmission.
*	Modules binned by current to improve system performance.
*	Superior module efficiency as per international benchmarks.
*	1500 VDC system voltage for lower BoS cost Salt mist, blowing sand and hail resistant.
*	Sustain heavy wind & snow loads (2400 pa & 5400 pa).
*	PID resistant with long term reliability.
*	Specs – 40wp – 410wp
*	Environmentally friendly energy.
*	Reduce Energy Bills.
*	25 Years Guarantee.
*	Innovative Technology.
*	Infinite Energy.

## Solar Grid-tie Inverter ##
### Brands Available- Waaree, Luminous, Okaya ###

*	Wide MPPT Range.
*	Maximum Efficiency 98.3%.
*	Multiple-monitoring Method (Wi-Fi/GPRS, RS485, WLAN).
*	IP 65 dust-proof and water-proof.
*	Anti- Islanding Protection Disconnects the inverter from grid during power failure.
*	Fanless low noise design.
*   Specs – 1kw – 100kw
*	Minimum loss of electricity due to high efficiency.
*	Stable and adequate supply of electricity.
*	5+ Years warranties upto 25years expandable warranty schemes.
*	Innovative Technology.
*	Cleaner and Healthier Energy.


## Solar Off-grid Inverters (PCU’s) ##
### Brands Available- Luminous ###

*	Pure Sine Wave Output.
*	Built-in MPPT Charge Controllers.
*	Smart Overload, Short Circuit Sensing and other Built-in protections.
*	Priority Selection for battery charging.
*	Independent of Grid (Grid is not mandatory).
*	LCD display to read and edit parameters.
*	Specs – 1kw – 10kw
*	Saves upto 90% of Grid Energy Usage.
*	Reduce Energy Bills.
*	Zero/Negligible Maintenance.
*	Much Better performance as compared to normal UPS.
*	Long Power Backup.



## Solar UPS ##
### Brand Available – Luminous, Okaya ###
 
*	Wide PWM/MPPT Range.
*	Pure Sine Wave Output.
*	Fast Charging algorithm.
*	User defined settings.
*	ECO and UPS mode available.
*	ISOT (Intelligent Solar Optimization Technique) Technology.
*	Specs – 350VA – 2035VA
*	Minimizes Grid Usage Upto zero.
*	Stable and adequate supply of electricity.
*	Supplies electricity during power cuts.
*	Increases Battery Life, connected to the system.
*	Multiple charging options available.

## Solar Battery##
### Brand Available – Luminous, Okaya###
 
*	Tubular technology for better life.
*	Rugged Performance.
*	Very Low Maintenance.
*	High Temperature Performance.
*	Better Performance on High DOD.
*	Upto 5 Years Warranty.
*	Specs – 20Ah – 200Ah


## Solar On-Grid System##
### Brand Available – Customisable###
 
*	Uses Sun Power to generate electricity from the unused Roof sapce.
*	Maximum Efficiency upto 98.3%.
*	Multiple-monitoring Method (Wi-Fi/GPRS, RS485, WLAN).
*	More than 25years performance.
*	Reduces Carbon footprints.
*	Zero/Negligible Maintenance.
*	Specs – As per requirement
*	Minimum loss of electricity due to high efficiency.
*	Can Sell Electricity to Discom.
*	Reduces Electricity Bills
*	Innovative Technology.
*	Cleaner and Healthier Energy.

## Solar Off-grid System ##
### Brand Available – Customisable###
 
*	Glass with anti-reflective coating improves transmission.
*	Modules binned by current to improve system performance.
*	Superior module efficiency as per international benchmarks.
*	1500 VDC system voltage for lower BoS cost Salt mist, blowing sand and hail resistant.
*	Sustain heavy wind & snow loads (2400 pa & 5400 pa).
*	PID resistant with long term reliability.
*	Specs – As per requirement
*	Zero Running Cost.
*	Remote Operation and monitoring possible.
*	Effective Power Management.
*	It reduces the grid usage upto Zero.
*	Absolutely Green Technology.


## Solar Water Heater##
### Brand Available – Waaree###
 
*	Attain 65deg – 85deg of Hot water
*	Centralized system for all washrooms connected.
*	Cost efficient as compared to other options available. 
*	Environment friendly.
*	Can be used for Industrial purpose.
*	Longer Life.
*	Specs – 100Ltr – 500Ltr (or as per requirement)
*	Minimum loss of electricity due to high efficiency.
*	Stable and adequate supply of electricity.
*	Very Low maintenance costs.
*	Simple to construct and install.
*	Saves time, as it gives continuous hot water supply.



## Solar Street Light##
### Brand Available – ARM Make###
 
*	Automatic dusk to dawn operations.
*	Cable free operation.
*	The variability of working hours.
*	High Transmission of Lights.
*	Intelligent Current Control.
*	Start without Delay.
*	Specs – 9Watts – 100Watts
*	Tremendous Energy Saving.
*	No pollution to Power Network.
*	High Luminous Efficiency.
*	Innovative Technology.
*	Independent from the Utility Grid.


## Solar Water Pump##
### Brand Available – Waaree###
 
*	No Fuel Cost.
*	No Electricity Required.
*	Long Operating Life.
*	Easy to Operate and Maintain.
*	Eco-friendly.
*	High Reliable and Durable.
*	Specs – 1HP – 5HP
*	Extremely Low Operating Cost.
*	Comparatively Low Maintenance.
*	Economically Beneficial.
*	Productivity Increases in Times of Need.
*	Easy to Transport & Relocate.



## Solar Structure##
### Brand Available – ARM Make###

*	MS Fabricated
*	Pre-galvanized
*	Hot-Dip Galvanized
*	Wide MPPT Range.
*	Maximum Efficiency 98.3%.
*	Multiple-monitoring Method (Wi-Fi/GPRS, RS485, WLAN).
*	IP 65 dust-proof and water-proof.
*	Anti- Islanding Protection Disconnects the inverter from grid during power failure.
*	Fanless low noise design.
*	Specs – 1kw – 100kw
*	Minimum loss of electricity due to high efficiency.
*	Stable and adequate supply of electricity.
*	5+ Years warranties upto 25years expandable warranty schemes.
*	Innovative Technology.
*	Cleaner and Healthier Energy.


## Solar Distribution Box##
### Brand Available – ARM Make###
 
*	Wide MPPT Range.
*	Maximum Efficiency 98.3%.
*	Multiple-monitoring Method (Wi-Fi/GPRS, RS485, WLAN).
*	IP 65 dust-proof and water-proof.
*	Anti- Islanding Protection Disconnects the inverter from grid during power failure.
*	Fanless low noise design.
*	Specs – 1kw – 50kw
*	Minimum loss of electricity due to high efficiency.
*	Stable and adequate supply of electricity.
*	5+ Years warranties upto 25years expandable warranty schemes.
*	Innovative Technology.
*	Cleaner and Healthier Energy.

## Household Wires##
### Brand Available – Waacab ###
 
*	Wide MPPT Range.
*	Maximum Efficiency 98.3%.
*	Multiple-monitoring Method (Wi-Fi/GPRS, RS485, WLAN).
*	IP 65 dust-proof and water-proof.
*	Anti- Islanding Protection Disconnects the inverter from grid during power failure.
*	Fanless low noise design.
*	Specs – 1mm2 – 6mm2
*	Minimum loss of electricity due to high efficiency.
*	Stable and adequate supply of electricity.
*	5+ Years warranties upto 25years expandable warranty schemes.
*	Innovative Technology.
*	Cleaner and Healthier Energy.

![](images/waacab_1.png)
![](images/waacab_2.png)
